include("MNIST.jl")

import .MNIST
import Random: MersenneTwister, shuffle!
using Serialization

"""
Classic [sigmoid](https://en.wikipedia.org/wiki/Sigmoid_function) activation function.
σ(x) = 1 / (1 + exp(-x))
https://discourse.julialang.org/t/sigmoid-function-in-julia/29594/3
"""
σ(x::Real) = one(x) / (one(x) + exp(-x))
const sigmoid = σ

σ′(x::Real) = σ(x) * (one(x) - σ(x))
const sigmoid_prime = σ′

C′(a, y) = a - y
const cost_derivative = C′

mutable struct Network
    layers
    sizes
    biases
    weights

    Network(sizes) = new(
        length(sizes),
        sizes,
        [randn(y) for y in sizes[2:end]],
        [randn(y, x) for (x, y) in zip(sizes[1:end-1], sizes[2:end])]
    )
end

function feedforward(nn::Network, a::Vector{Float64})
    for (w, b) in zip(nn.weights, nn.biases)
        a = σ.(w * a + b)
    end
    return a
end

function stochastic_gradient_descent(nn::Network, traindata, epochs, mini_batch_size, η, testdata=nothing)
    n = size(traindata, 1)

    for epoch in 1:epochs
        shuffle!(traindata)
        foreach(k -> update_mini_batch!(nn, view(traindata, k:k + mini_batch_size - 1), η), 1:mini_batch_size:n)

        if testdata != nothing
            println(stderr, "Epoch $epoch: $(evaluate(nn, testdata)) / $(length(testdata))")
        end
    end
end

function update_mini_batch!(nn::Network, mini_batch, η)
    ∇b = zero.(nn.biases)
    ∇w = zero.(nn.weights)

    for z in mini_batch
        x, y = z
        ∇bₓ, ∇wₓ = backprop(nn, x, y)
        ∇b += ∇bₓ
        ∇w += ∇wₓ
    end

    m = size(mini_batch, 1)
    nn.biases -= η / m * ∇b
    nn.weights -= η / m * ∇w
end

function backprop(nn::Network, x, y)
    a = x
    as = [x]
    zs = []

    for (b, w) in zip(nn.biases, nn.weights)
        z = w * a + b
        push!(zs, z)
        a = σ.(z)
        push!(as, a)
    end

    δ = C′(as[end], y) .* σ′.(zs[end])

    ∇b = zero.(nn.biases)
    ∇w = zero.(nn.weights)
    ∇b[end] = δ
    ∇w[end] = δ * as[end - 1]'

    L = nn.layers
    # for l = L-2:-1:2
    for l = 1:L-2
        δ = (nn.weights[end - l + 1]' * δ) .* σ′.(zs[end - l])
        ∇b[end - l] = δ
        ∇w[end - l] = δ * as[end - l - 1]'
    end

    ∇b, ∇w
end

function evaluate(nn::Network, data)
    #=
    x, y = data[1]
    MNIST.renderimage(x)
    println(stderr, argmax(feedforward(nn, x)))
    println(stderr, argmax(y))
    =#
    count = 0
    for (x, y) in data
        if argmax(feedforward(nn, x)) == argmax(y)
            count += 1
        end
    end
    # sum(argmax(feedforward(nn, x)) == argmax(y) for (x, y) in data)
    count
end

biases(nn::Network) = nn.biases
weights(nn::Network) = nn.weights

#=
nn = Network([784, 15, 10])
stochastic_gradient_descent(nn, traindata, 30, 10, 3.0, testdata)
serialize("./data/mnist.nn", nn)
=#

#=

images = MNIST.readimages("./data/t10k-images-idx3-ubyte", 1)
labels = MNIST.readlabels("./data/t10k-labels-idx1-ubyte", 1)
# MNIST.renderimage(image)

image = images[:, 1]
label = labels[:, 1]
prediction = feedforward(network, image)
println(prediction - label)
=#

#=
x = MNIST.readimages("./data/train-images-idx3-ubyte")
y = MNIST.readlabels("./data/train-labels-idx1-ubyte")

trainx = view(x, :, 1:50_000)
trainy = view(y, :, 1:50_000)
testx = view(x, :, 50_001:60_000)
testy = view(y, :, 50_001:60_000)

@show size(trainx), size(trainy), size(testx), size(testy)
sgd(nn, (trainx, trainy), 2, 1000, 3.0, (testx, testy))
# show(stderr, "text/plain", A)
=#

# data = MNIST.loaddata("./data/train-images-idx3-ubyte", "./data/train-labels-idx1-ubyte")
data = MNIST.loaddata("./data/t10k-images-idx3-ubyte", "./data/t10k-labels-idx1-ubyte")
nn = deserialize("./data/mnist.nn")
evaluation = evaluate(nn, data)
@show length(data) evaluation

#=
traindata = view(data, 1:50_000)
testdata = view(data, 50_001:60_000)

# #=
x, y = testdata[1]
MNIST.renderimage(x)
println(stderr, argmax(y) - 1)
# =#
=#
