# unn
A μ-Neural-Networks library for Julia from scratch.
Programmed on: 2022-08-22

## References
- <http://neuralnetworksanddeeplearning.com>
- <https://github.com/analog-hors/mnist-digit-nn>
- <https://github.com/iannisdezwart/AI_NN>
- <https://en.wikipedia.org/wiki/One-hot>
- <https://fluxml.ai/Flux.jl/stable/data/onehot/>
- <https://discourse.julialang.org/t/all-the-ways-to-do-one-hot-encoding/64807>

## Resources
- <http://mathoverflow.net/questions/25983/intuitive-crutches-for-higher-dimensional-thinking>
