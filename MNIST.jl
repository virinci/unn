module MNIST

export renderimage, readimages, readlabels, loaddata

import SparseArrays: spzeros

function renderimage(image::Vector{UInt8})
    print("P5\n28 28\n255\n")
    @assert size(image) == (784,)
    write(stdout, image)
end

function renderimage(image::Vector{Float64})
    print("P5\n28 28\n255\n")
    @assert size(image) == (784,)
    write(stdout, map(x -> round(UInt8, x * 0xFF), image))
end

function renderimage(image)
    print("P5\n28 28\n255\n")
    @assert size(image) == (784,)
    write(stdout, map(x -> round(UInt8, x * 0xFF), image))
end

function readimages(s::IO, n::Union{Int, Nothing}=nothing)
    @assert position(s) == 0

    if n != nothing
        seek(s, 16 + 784 * (n - 1))
        images = read(s, 784)
        count = 1
    else
        seek(s, 16)
        images = read(s)
        count = size(images, 1) ÷ 784

        @assert size(images, 1) == 784 * 10_000 || size(images, 1) == 784 * 60_000
        @assert count == 10_000 || count == 60_000
    end

    reshape(images, 784, count) / 0xFF
end

function readlabels(s::IO, n::Union{Int, Nothing}=nothing)
    @assert position(s) == 0

    if n != nothing
        seek(s, 8 + (n - 1))
        labels = read(s, 1)
        count = 1
    else
        seek(s, 8)
        labels = read(s)
        count = size(labels, 1)

        @assert size(labels, 1) == 10_000 || size(labels, 1) == 60_000
        @assert count == 10_000 || count == 60_000
    end

    onehotbatch(labels, 0:9)
end

function readimages(filename::AbstractString, args...)
    open(io -> readimages(io, args...), filename)
end

function readlabels(filename::AbstractString, args...)
    open(io -> readlabels(io, args...), filename)
end

function onehot(x, labels)
    n = findfirst(z -> z == x, labels)
    @assert n != nothing
    A = spzeros(Float64, 10)
    A[n] = 1.0
    A
end

function onehotbatch(xs, labels)
    A = spzeros(size(labels, 1), size(xs, 1))
    for (i, x) in enumerate(xs)
        n = findfirst(z -> z == x, labels)
        @assert n != nothing
        A[n, i] = 1.0
    end
    A
end

function loaddata(fi::IO, fl::IO)
    @assert position(fi) == 0 && position(fl) == 0

    seek(fi, 16)
    seek(fl, 8)

    images = read(fi)
    labels = read(fl)

    # @assert length(images) ÷ 784 == 60_000 && length(labels) == 60_000

    count = length(labels)

    images = reshape(images, 784, count)
    # renderimage(images[:, 3] / 0xFF)

    [(images[:, i] / 0xFF, onehot(labels[i], 0:9)) for i in 1:count]
end

function loaddata(fi::AbstractString, fl::AbstractString)
    open(fi) do fi
        open(fl) do fl
            loaddata(fi, fl)
        end
    end
end

end
